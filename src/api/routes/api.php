<?php

declare(strict_types=1);

// Here should be located public routes.

use Api\Http\Controllers\QuestionnaireController;

Route::group(['middleware' => config('api.middleware')], function() {
    Route::group(['middleware' => 'auth:api'], function () {
        // Here should be located routes fot logged in users only.
        Route::post('/gdpr/risks/sign', [QuestionnaireController::class, 'sign'])
            ->name('auth.risks.sign');
    });
});
