<?php
declare(strict_types=1);

namespace Api\Http\Controllers;

use Api\Forms\QuestionnaireForm;
use Illuminate\Http\Request;

/**
 * Class QuestionnaireController
 *
 * @package Api\Http\Controllers
 */
class QuestionnaireController extends Controller
{
    /**
     * Sign
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sign(Request $request)
    {
        app(QuestionnaireForm::class, ['data' => $request->all()])->process();

        return $this->jsonSuccess(['message' => 'Risks understanding signed successfully']);
    }
}
