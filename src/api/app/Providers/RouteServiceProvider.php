<?php

namespace Api\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

/**
 * Class RouteServiceProvider
 *
 * @package Api\Providers
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * Api Namespace
     *
     * @var string
     */
    protected $controllerNamespace = 'Api\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::middleware(config('api.middleware'))
            ->namespace($this->controllerNamespace)
            ->as('api::')
            ->prefix('v1')
            ->group(base_path('api/routes/api.php'));
    }
}
