<?php

declare(strict_types=1);

namespace Admin\Services\Menu;

use LenderKit\Admin\Services\Menu\AdminMenuExtender;
use LenderKit\Admin\Services\Menu\Menu;

/**
 * Class SidebarMenuExtender
 *
 * @package Admin\Services\Menu
 */
class SidebarMenuExtender extends AdminMenuExtender
{
    /**
     * Menu Items
     *
     * @param Menu $menu
     *
     * @return mixed|void
     */
    public function menuItems(Menu $menu)
    {
        // TODO: add your menu items
        //$menu->find('community')->submenu()->add(...);
    }
}
