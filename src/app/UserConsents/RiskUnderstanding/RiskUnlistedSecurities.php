<?php
declare(strict_types=1);

namespace App\UserConsents\RiskUnderstanding;

/**
 * Class RiskUnlistedSecurities
 *
 * @package App\UserConsents\RiskUnderstanding
 */
class RiskUnlistedSecurities extends BaseRiskUnderstandingConsent
{
    /**
     * @var string
     */
    protected $key = 'risk_unlisted_securities';
}
