<?php
/**
 * @copyright Copyright (c) JustCoded Ltd. All Rights Reserved.
 *    Unauthorized copying of this file, via any medium is strictly prohibited.
 *    Proprietary and confidential.
 *
 * @license https://lenderkit.com/license
 * @see https://lenderkit.com/
 *
 * @package LenderKit\Core
 */

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use LenderKit\Modules\RBAM\Models\PermissionPolicy;
use LenderKit\Models\{Permission, Role};
use LenderKit\Traits\Common\CreatesAdminUsers;

/**
 * Class PoliciesSeeder
 *
 * This is example of policies and this file should be removed or updated for commercial project.
 * (this file contain permissions from different modules)
 */
class PoliciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        foreach ($this->seeds() as $seed) {
            $permissionNames = Arr::pull($seed, 'permissions');

            $permissions = Permission::whereIn('name', $permissionNames)->get();
            if (! $permissions->count()) {
                continue;
            }

            $policy = PermissionPolicy::updateOrCreate($seed, $seed);
            $policy->syncPermissions($permissions);
        }
    }

    /**
     * Seeds
     *
     * @return array|array[]
     */
    protected function seeds(): array
    {
        return [
            [
                'title'       => 'Customer details edit policy',
                'subsystem'   => Role::SUBSYSTEM_ADMIN,
                'permissions' => [
                    'core::admin:users.all_users_list.view',
                    'core::admin:users.user_details.view',
                    'core::admin:users.user_details.view.history',
                    'core::users.create',
                    'core::users.update',
                    'users_grids::admin:users.individual_brokers_list.view',
                    'users_grids::admin:users.individual_fundraisers_list.view',
                    'users_grids::admin:users.individual_investors_list.view',
                    'users_grids::admin:users.legal_brokers_list.view',
                    'users_grids::admin:users.legal_fundraisers_list.view',
                    'users_grids::admin:users.legal_investors_list.view',
                    'core::admin:users.addresses.view',
                    'core::users.addresses.create',
                    'core::users.addresses.delete',
                    'core::users.addresses.update',
                    'core::admin:users.documents.view',
                    'core::users.documents.delete',
                    'core::users.documents.create',
                    'core::users.documents.update',
                    'core::admin:users.tags.view',
                    'core::users.tags.update',
                    'core::admin:organizations.all_organizations_list.view',
                    'core::admin:organizations.organization_details.view',
                    'core::admin:organizations.organization_details.view.history',
                    'core::organizations.create',
                    'core::organizations.update',
                    'organizations_grids::admin:organizations.brokering_organizations_list.view',
                    'organizations_grids::admin:organizations.fundraising_organizations_list.view',
                    'organizations_grids::admin:organizations.investing_organizations_list.view',
                    'core::admin:organizations.addresses.view',
                    'core::organizations.addresses.create',
                    'core::organizations.addresses.delete',
                    'core::organizations.addresses.update',
                    'core::admin:organizations.documents.view',
                    'core::organizations.documents.create',
                    'core::organizations.documents.delete',
                    'core::organizations.documents.update',
                    'core::admin:organizations.tags.view',
                    'core::organizations.tags.update',
                ],
            ],
            [
                'title'       => 'Investment and transactions details edit policy',
                'subsystem'   => Role::SUBSYSTEM_ADMIN,
                'permissions' => [
                    'core::admin:users.all_users_list.view',
                    'core::admin:users.user_details.view',
                    'core::admin:users.user_details.view.billing_info',
                    'users_grids::admin:users.individual_brokers_list.view',
                    'users_grids::admin:users.individual_fundraisers_list.view',
                    'users_grids::admin:users.individual_investors_list.view',
                    'users_grids::admin:users.legal_brokers_list.view',
                    'users_grids::admin:users.legal_fundraisers_list.view',
                    'users_grids::admin:users.legal_investors_list.view',
                    'core::admin:users.documents.view',

                    'core::admin:organizations.all_organizations_list.view',
                    'core::admin:organizations.organization_details.view',
                    'core::admin:organizations.organization_details.view.billing_info',
                    'organizations_grids::admin:organizations.brokering_organizations_list.view',
                    'organizations_grids::admin:organizations.fundraising_organizations_list.view',
                    'organizations_grids::admin:organizations.investing_organizations_list.view',
                    'core::admin:organizations.documents.view',

                    'core::admin:offerings.all_offerings_list.view',
                    'core::admin:offerings.offering_details.view',

                    'core::admin:investments.all_investments_list.view',
                    'core::admin:investments.investment_details.view',
                    'core::admin:investments.investment_details.view.history',
                    'core::investments.create',
                    'core::investments.update',
                    'debt::admin:investments.debt_investments_list.view',
                    'donation::admin:investments.donation_investments_list.view',
                    'equity::admin:investments.equity_investments_list.view',
                    'core::admin:investments.documents.view',
                    'core::investments.documents.create',
                    'core::investments.documents.delete',
                    'core::investments.documents.update',

                    'core::admin:transactions.all_transactions_list.view',
                    'core::admin:transactions.transaction_details.view',
                    'core::admin:transactions.transaction_details.view.history',
                    'core::transactions.create',
                    'core::transactions.update',

                    'core::admin:wallets.all_wallets_list.view',
                    'core::admin:wallets.wallet_details.view',
                    'core::admin:wallets.platform_wallet_details.view', // under question?

                    'core::payouts.update',
                    'debt::admin:payouts.debt_payout_details.view',
                    'debt::admin:payouts.debt_payouts_list.view',
                    'equity::admin:payouts.equity_payouts_list.view',
                    'equity::admin:payouts.equity_payout_details.view',

                    'secondary_market_debt::admin:deals.debt_deals_list.view',
                    'secondary_market_equity::admin:deals.equity_deals_list.view',
                    'secondary_market::admin:deals.all_deals_list.view',
                    'secondary_market::deals.create',
                    'secondary_market::deals.update',

                    'auto_investment::admin:auto_investment_assignments.assignments_list.view',
                    'auto_investment::admin:auto_investment_settings.view',
                    'auto_investment::admin:investments.auto_investments_list.view',
                    'auto_investment::admin:offerings.auto_investment_offerings_list.view',
                    'auto_investment::auto_investment_assignments.create',
                    'auto_investment::auto_investment_assignments.update',
                    'auto_investment::auto_investment_settings.update',
                ],
            ],
        ];
    }
}
