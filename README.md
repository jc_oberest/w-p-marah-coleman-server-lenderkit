# LenderKit Server Starter

LenderKit Server Starter is a skeleton crowdfunding application based on 
  [LenderKit Framework](https://bitbucket.org/justcoded/lenderkit-core)
  and [Laravel PHP Framework](https://laravel.com/). 

LenderKit Server provides:

* Public API to build WebApp portal or Mobile Application
* Admin panel for business owners to manage everything in detail
* Database to keep all records in place  

Full documenation is at [docs/README.md](docs/README.md)

## Requirements

* Linux server*
* Docker CE 19+ / Docker Compose 1.23.2+
* Domain name and SSL certificate for running on dedicated servers
* Understanding of Docker containers, GNU Makefiles, Nginx server configuration

You can read [detailed documentation](docs/README.md) to get information how to match these requirements.

_* Note: If you want to setup project on MacOS - please read detailed documentation as well._

## Code structure

In this document we will use 2 terms: **root level** and **application level**. 
Please take your attention on the scheme below: 

```
/                # ROOT LEVEL
 |- /build         # dev Docker and CI configuration  
 |- /configs       # containers configuration files
 |- /docs          # detailed documentation
 |- /runtime       # temporary files and docker containers storage volumes
    |- /mysql
    |- /redis
 |- /src         # APPLICATION LEVEL
    |- /admin            # admin panel specific code
    |- /api              # API specific code
    |- /app              # common app specific code
    |- /config           # app configs
    |- /database         # standard to Laravel
    |- /resources        # language files
    |- /storage          # standard to Laravel
    |- .env              # application level env configuration
    |- artisan           # API Laravel artisan runner
    |- Makefile          # application level Make helper
    |- composer.json      
 |- .env                  # root level env configuration
 |- docker-compose*.yml   # docker compose containers config files
 |- Makefile              # root level Make helper
```

## Product License

LenderKit Product is not open-source and you need to have license to be able to install and operate with LenderKit.

License key (and license file) can be obtained from your Account Manager.  
LenderKit developers can find license key in LenderKit [test development environment](https://admin.lenderkit.develop.s8.jc/license).  

## Installation 

### Installation helpers

Server installation is a complex process with numerous steps to initialize server application. To simplify this process
we use GNU Make utility and configured Makefile's.

There are **2 Makefile's** in the project:

* `/Makefile` or **root level Makefile**.  
This file contains targets to configure your server environment and operate with Docker containers. 
This one should be called **outside** any Docker container (If you're on Mac - inside Vagrant SSH).
* `/src/Makefile` or **application level Makefile**.  
This file contains helpers to operate with PHP application. It should be called **inside PHP containers** (app/queue/scheduler). 
`/src` folder is mounted as a docroot `/var/www/html` to docker php containers, so it's the only available Makefile inside the containers. 
In general this Makefile contains application install/update scripts and `artisan` calls.

### Quick install

To init docker containers and run LenderKit project you can use root level Makefile to do everything for you.

#### 0. Clone/Download

* Download project source to some folder
* If you have **license file**, then copy save it as `src/bootstrap/license`. Don't commit this file to git!!!  
_if you don't have the license file, you will be asked for license key during the installation process._

#### 1. Init your environment

After download you need to init your project with your own docker-configuration `/.env` file, docker-compose files and nginx server config.

To copy all necessary files and get information where you can find your local configs run:

`make init-dev` (for DEVELOPMENT) **OR** `make init` (for client server). 

By default, **you have the working configuration files** and you can leave everything as is. Xdebug and Scheduler are turned off by default (you can turn on them later).

**Environment variables**

Inside docker `/.env` file you can configure:

* `APP_ID` - can be used inside `docker-compose` files to customize some volumes mapping to project-based specific folders.
* `QUEUE_ENABLED`, `SCHEDULER_ENABLED` - Enable/Disable Queue container (`supervisord` with `artisan queue:work`) and Scheduler container (cron with `artisan schedule:run`)
* `HOST_*` - configuration of external mapped ports for development purposes (like port to connect to MySQL from PHPStorm)
* `SSHD_ENABLED`, `PHP_XDEBUG_ENABLED` - enables SSH server daemon on PHP-FPM container (so you can connect to it via PHPStorm) and enable XDEBUG php extension.
* `PHP_IDE_CONFIG` - contains your server host info, used to find correct Server configuration inside PHPStorm to map files during debug.

**Docker compose**

There are 2 docker-compose main files, which will be initialized:

* `/docker-compose.yml`
* `/docker-compose.override.yml` which is merged automatically by docker compose utility.  
This file contains special configuration for development environment (like XDEBUG envs, extra volumes, etc.)

After you init your environment variables and configure your required ports (inside `.env`, `docker-compose.yml` and `configs/nginx-server.conf`)
you can do a test-run to check that your docker-compose is correct:

#### 2. Docker test run

* First you need to do a docker login to the [private docker hub](https://hub.jcdev.net) with your username/password (ask Server Administrator for it).  
    * `docker login hub.jcdev.net:24000` - for node.js generic image
    * `docker login hub.jcdev.net:24200` - for LenderKit official PHP image

* Then you can do a test run.  
`make test-run`

On a test-run:

* Will be generated MySQL database and user
* Queue/Scheduler containers will fail (because we don't have composer/vendors installed)

Wait for "db" container to stop writing output to your console. Last messages should be that it's ready for connection on port 3306.

Now exit your test-run (Ctrl+C) and go to the installation step.

#### 3. Installing LenderKit

By default, installation will run with default `src/.env` variables (copied from `src/.env.example`).  
If you have custom domain or special settings for database, cache storage, etc. you need to copy and edit `src/.env` 
manually before running the installation.

To install the project after `init` and `test-run` just run:

```bash
make install
```

During the the installation process you will be asked you to enter **license key**, if you didn't uploaded license file on start. 

#### 4. Site Domain and Access

On real server you need to configure `configs/nginx-server.conf`, `configs/ssl` for your domains and certificates. 

On localhost you need to point your project domain to a localhost inside `/etc/hosts` file. 
By default, domain is `lenderkit.test`, so you need to add this to your hosts file. Run on your machine _(for Mac users - NOT Vagrant)_:

```bash
sudo bash -c 'echo "127.0.0.1 lenderkit.test" >> /etc/hosts'
``` 

_or just edit this file with editor: `sudo nano /etc/hosts`_

By default, site is accessible within such URLs:

* Admin: https://lenderkit.test:8443
* API: https://lenderkit.test:8888/v1/docs
* Mailhog (test mail server): http://lenderkit.test:8445

If you installed project on localhost you have admin user: admin@gmail.com / admin.

## Start, stop, update

If you reboot your server and don't have auto-start options in your docker-compose config, then you need to launch
docker containers again. To do this just run:

```bash
make run
```

If you want to stop project (update or freeze ports for example) you need another command:

```bash
make stop
```

To update your project to a newer version downloaded from git you can run special command after `git pull` (on **root level**):

```bash
make update
```

## Docker containers

Default installation will launch such containers:

* `app` - php-fpm, you can run all your artisan scripts inside this container
* `queue` - php-fpm with supervisord, running `php artisan queue:work`
* `scheduler` - php-from with a crontab, running `php artisan schedule:run` every 5 minutes
* `db` - MySQL 5.7 container, have mapping of a DB storage to `/runtime/mysql`
* `redis` - Redis 5.0, have mapping of a DB storage to `/runtime/redis`
* `web` - nginx, pointing admin and api ports to `app` container
* `nodejs` - Node.js 10 for building admin css/scripts
* `mailhog` - (DEBUG ONLY) fake smtp server, which catch all outgoing mail and provides web interface for them

### APP container: running artisan and composer

#### Composer

All composer commands should be executed with composer memory limit option. For example:

`COMPOSER_MEMORY_LIMIT=2G composer install`

To simplify composer usage you can run composer from a make utility like this:

```bash
make composer-install   #or make ci
make composer-update    #or make cu
make composer-autoload  #or make ca
```

#### Artisan

By default `php artisan` command will be launched as API application. So admin commands won't be available here 
(as well as admin vendor:publish options). To run admin artisan simply run `php admin/artisan`.

Furthermore, application have 2 different bootstrap folders and 2 different bootstrap caches. So if you want to clean
your cache you need to run 2 commands, one for API and second for the Admin.

## Read more

If you plan to develop this project we suggest to read all materials available in our [Detailed Documentation](docs/README.md). 
