# LenderKit-based Project

[LenderKit](https://lenderkit.com/) is an enterprise-level white-label crowdfunding platform developed by [JustCoded](https://justcoded.com/).

LenderKit-based Project is a server-side solution for crowdfunding based on 
  [LenderKit Framework](https://bitbucket.org/justcoded/lenderkit-core)
  and [Laravel PHP Framework](https://laravel.com/). 

LenderKit Server provides:

* Public API to build WebApp portal or Mobile Application
* Admin panel for business owners to manage everything in detail
* Database to keep all records in place  

## Product License

LenderKit Product is not open-source and you need to have license to be able to install and operate with LenderKit.

License key (and license file) can be obtained from your Account Manager.    
LenderKit developers can find license key in LenderKit [test development environment](https://admin.lenderkit.develop.s8.jc/license).  

## Getting Started

* [Before You start](before-you-start.md). Get basic knowledge of technologies being used.
* [Requirements](requirements.md). Know how to meet server requirements.
* [Installation](installation.md) +-
* [Docker Containers](docker-containers.md)
* [Makefiles Reference](makefile-reference.md)
* [Starting New Project](start-new-project.md)

## Digging Deeper

* [Docker Containers Details](docker-containers-details.md) -
    * [PHP Container](php-container.md) +-
* [Running Unit Tests](running-unit-tests.md)
* [PHPStorm Configuration](phpstorm-configuration.md) for development
* [Troubleshooting](troubleshooting.md) -
