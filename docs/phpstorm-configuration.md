# PHPStorm

To debug PHPStorm you need to:

[TOC]

Imagine that we set up our site on the domain **crowdfunding.test**. Below we will use this domain in different 
settings. Replace the examples below with your own domain.

Furthermore, you have free port 42222.

## Enabling XDebug

* Inside `/.env` file update such variables:
```
SSHD_ENABLED=1

HOST_SSHD_PORT=42222

PHP_XDEBUG_ENABLED=1
PHP_IDE_CONFIG=serverName=crowdfunding.test
```
* Rerun your container: `make run`

## Configure PHP Interpreter

* Open _Settings > Languages & Frameworks > PHP_
* Press **[...]** near CLI interpreter
* Press **[+]** to add a new one, select "Vagrant, Docker, Remote"
* In new popup select "SSH", enter such credentials
    * Host: `crowdfunding.test` (your host)
    * Port: `42222` (port specified in your .env file)
    * User: `root`
    * Pass: `docker`
    * Path: `/usr/local/bin/php`  
    ![creating ssh connection](images/creating-ssh-connection.png)
* Save and select new profile as CLI interpreter. You should see PHP and XDebug versions:
    ![created ssh connection](images/created-ssh-connection.png)

After that:

* Open _Settings > Languages & Frameworks > PHP > Debug_
* Set XDebug Port to `10000`

## Configure Server

* Open _Languages & Frameworks > PHP > Servers_
* Press **[+]** to add a new one
* Enter such details:
    * Name: `crowdfunding.test` (your host)
    * Host: `crowdfunding.test` (your host)
    * Port: 8443 (your admin host)
* Enable path mapping and point `/src` to `/var/www/html`

![configure php server](images/php-server-config.png)

_* Note: If you have problems connecting to container SSH because of wrong RSA/ECSMA keys, 
you can clean them with a command `ssh-keygen -R [crowdfunding.test]:42222`._

## Test XDebug

Enable Zero debug, add breakpoint inside `src/admin/public/index.php` for example and open admin panel:
http://crowdfunding.test:8443.

## Unit Tests

* Make sure you have correct version of PHPUnit inside `composer.json`: `"phpunit/phpunit": "8.5.2"`
* Open your `/src/.env` file and update such values:
    ```
    DB_HOST_TESTING=db
    DB_DATABASE_TESTING=lenderkit_test
    DB_USERNAME_TESTING=lenderkit
    DB_PASSWORD_TESTING=lenderkit
    DB_PREFIX_TESTING=test_
    ```
    By default, these values have docker container variables set, which are populated from docker-compose file.
    For some reason, when you launch tests from PHPStorm these variables are not available.
* Initialize test database from **root level** with command `make phpunit-init`
* In your PHPStorm open _Settings > Languages & Frameworks > PHP > Test Frameworks_
* Press **[+]** to add a new one and select _PHPUnit by remote interpreter_, then select PHP Interpreter created 
before that.
* Set path mapping from `src` to `/var/www/html`. You need to set **absolute path** to your src folder
    ![phpunit path mapping](images/phpunit-path-mapping.png)
* Select composer autoload file location from browse window. You should see PHPUnit 8.5.2 version.
* Set _Default configuration file_ from browse window to project `phpunit.xml` file.
    ![phpunit connected](images/phpunit-connected.png)
* That's it, you can now run test/Feature/ApiDocsTest from PHPStorm to make sure all is configured correctly

## NPM

To develop admin scripts and assets you will need npm watch mode.

To make it works faster you need:

* Turn off "Safe write" mode in _Settings > Appearance & Behavior > System Settings"
* Check the amount of available file watchers with command: `cat /proc/sys/fs/inotify/max_user_watches`
    * If this amount is less than 100000, then run `make init-watch`
* **For MacOS**: Remove file limits running in your Mac terminal `sudo launchctl limit maxfiles unlimited unlimited`

Watch mode is working slow at start. Over time, it will watch for changes faster, so please be patient.
